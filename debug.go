package main

import (
	"fmt"
)

const (
	NO_DEBUG_LEVEL = iota
	WARNING_LEVEL
	DEBUG_LEVEL
	INFO_LEVEL
)

var GLOBAL_debug_level int
var msg_level map[int]string

func debug(message string,level int) {
	if level <= GLOBAL_debug_level{
        output := msg_level[level] + ": " + message
		fmt.Println(output)
	}
}

func init() {
	msg_level = make(map[int]string)
	msg_level[1] = "WARN"
	msg_level[2] = "DEBUG"
	msg_level[3] = "INFO"
}
