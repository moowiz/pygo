package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

var reader *bufio.Reader
var input_file *os.File

func In(it interface{}, arr interface{}) (found bool) {
	change_arr, ok := arr.([]interface{})
	if !ok {
		return false
	}
	for _, v := range change_arr {
		if v == it {
			return true
		}
	}
	return false
}

func getInput(prompt string) (string, error) {
	var line string
	var err error
	if input_file == os.Stdin {
		fmt.Print(prompt)
	}
	line, err = reader.ReadString('\n')
	if err == io.EOF {
		return eof_s, nil
	}
	if err != nil {
		return "", err
	}
	return strings.TrimRight(line, " \n"), nil
}

func InitUtils(filename string) (err error) {
	if len(filename) > 0 {
		input_file, err = os.Open(filename)
		if err != nil {
			return err
		}
	} else {
		input_file = os.Stdin
	}
	reader = bufio.NewReader(input_file)
	return nil
}

func count_spaces(s string) int {
	space := ""
	for strings.HasPrefix(s, space) {
		space += " "
	}
	return len(space) - 1
}
