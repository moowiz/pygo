package main

import (
	"fmt"
	"math/big"
	"strconv"
)

type parser struct {
	stmts       chan stmt
	next_tok    *token
	lex         *lexer
	has_new     bool
	should_emit chan bool
}

func (p *parser) parse() {
	cont := false
	for {
		cont = false
		ts := p.peek_wait()
		debug(fmt.Sprintf("Got new token %#v", ts), INFO_LEVEL)
		if ts.kind == eof_token || ts.kind == eol_token {
			val := <-p.should_emit
			p.next()
			cont = true
			if val {
				if ts.kind == eof_token {
					p.stmts <- eof_stmt{}
				} else {
					p.stmts <- eol_stmt{}
				}
			}
		}
		if !cont {
			s, e := p.parseStmt()
			debug(fmt.Sprintf("Internally parsed %#v e %#v", s, e), DEBUG_LEVEL)
			should_i := <-p.should_emit
			if !should_i {
				for p.has_next() {
					p.next()
				}
				continue
			}

			if p.has_next() {
				next := p.next()
				if next.kind != eol_token {
					e = raiseException("Invalid Syntax")
				}
			}

			if e != nil {
				p.stmts <- error_stmt{e}
			} else {
				p.stmts <- s
			}
		}
	}
}

func (p *parser) parseParen() (*expr_lst, py_exception) {
	if p.peek().kind == rp_token {
		p.next()
		return nil, nil
	}

	pre_args, exc := p.parseExpr()
	if exc != nil {
		return nil, exc
	}
	args, ok := pre_args.(*expr_lst)
	if !ok {
		args = &expr_lst{[]expr{pre_args}}
	}

	_, exc = p.expect(rp_token)
	if exc != nil {
		return nil, exc
	}
	return args, nil
}

var rec int

func (p *parser) parseDef() (*def_stmt, py_exception) {
	//for now, ignore default args and *args, **kwds

	rec++
	_, exc := p.expect(def_token)
	if exc != nil {
		return &def_stmt{}, exc
	}

	id_tok, exc := p.expect(identifier_token)
	if exc != nil {
		return &def_stmt{}, exc
	}

	id := &identifier_expr{id_tok.content}
	_, exc = p.expect(lp_token)
	if exc != nil {
		return &def_stmt{}, exc
	}

	args, exc := p.parseParen()
	if exc != nil {
		return &def_stmt{}, exc
	}

	_, exc = p.expect(cln_token)
	if exc != nil {
		return &def_stmt{}, exc
	}

	_, exc = p.expect(eol_token)
	if exc != nil {
		return &def_stmt{}, exc
	}

	suite, exc := p.parseSuite()
	if exc != nil {
		return &def_stmt{}, exc
	}

	rec--
	return &def_stmt{id, &args_lst{args}, suite}, nil
}

func (p *parser) parseIf() (stmt, py_exception) {
	p.next()
	suites := make([]*suite, 0)
	tests := make([]expr, 0)
	flag := true
	for flag || p.peek().kind == elif_token {
		test, exc := p.parseExpr()
		if exc != nil {
			return nil, exc
		}
		tests = append(tests, test)

		_, exc = p.expect(cln_token)
		if exc != nil {
			return nil, exc
		}

		_, exc = p.expect(eol_token)
		if exc != nil {
			return nil, exc
		}

		next_suite, exc := p.parseSuite()
		if exc != nil {
			return nil, exc
		}
		suites = append(suites, next_suite)
		flag = false
	}
	var else_suite *suite
	var exc py_exception
	else_suite = &suite{}
	if p.peek().kind == else_token {
		else_suite, exc = p.parseSuite()
		if exc != nil {
			return nil, exc
		}
	}
	return &if_stmt{suites, tests, else_suite}, nil
}

func (p *parser) parseWhile() (stmt, py_exception) {
	p.next()
	test, exc := p.parseExpr()
	if exc != nil {
		return nil, exc
	}

	_, exc = p.expect(cln_token)
	if exc != nil {
		return nil, exc
	}

	_, exc = p.expect(eol_token)
	if exc != nil {
		return nil, exc
	}

	suite, exc := p.parseSuite()
	if exc != nil {
		return nil, exc
	}
	return &while_stmt{test, suite}, nil
}

func (p *parser) parseSuite() (*suite, py_exception) {
	_, exc := p.expect(indent_token)
	if exc != nil {
		return nil, exc
	}

	this_suite := &suite{}
	for p.peek().kind != dedent_token {
		if p.peek().kind == eol_token {
			p.next()
			continue
		}
		next, exc := p.parseStmt()
		if exc != nil {
			return nil, exc
		}

		this_suite.stmts = append(this_suite.stmts, next)
	}
	p.next()
	return this_suite, nil
}

func (p *parser) unexpectedToken(t *token, want *tokenType) py_exception {
	var exc py_exception
	if want != nil {
		exc = raiseException(fmt.Sprintf("Unexpected token %#v. Wanted %#v", t, want))
	} else {
		exc = raiseException(fmt.Sprintf("Unexpected token %#v", t))
	}
	for p.has_next() {
		p.next()
	}
	return exc
}

func (p *parser) parseOneExpr() (expr, py_exception) {
	debug(fmt.Sprintf("Parsing one expr %#v", p.peek()), INFO_LEVEL)
	t := p.next()
	switch t.kind {
	case int_token, float_token:
		num, err := parseNumber(t)
		if err != nil {
			return nil, err
		}
		return num, nil
	case lp_token:
		parsed_paren, exc := p.parseParen()
		if exc != nil {
			return nil, exc
		}
		if parsed_paren.lst == nil {
			return parsed_paren, nil
		}
		if len(parsed_paren.lst) == 1 {
			return parsed_paren.lst[0], nil
		}
		if len(parsed_paren.lst) == 2 && parsed_paren.lst[1] == nil {
			return &expr_lst{parsed_paren.lst[:1]}, nil
		}
		return parsed_paren, nil
	case identifier_token:
		var rval expr
		rval = &identifier_expr{t.content}
		if p.has_next() && p.peek().kind == lp_token {
			for p.peek().kind == lp_token {
				p.next()
				args, exc := p.parseParen()
				if exc != nil {
					return nil, exc
				}
				rval = &call_expr{rval, &args_lst{args}}
			}
		}
		return rval, nil
	}
	return nil, p.unexpectedToken(t, nil)
}

func (p *parser) parseExpr() (expr, py_exception) {
	debug(fmt.Sprintf("parsing expr %#v", p.peek()), INFO_LEVEL)
	exp, exc := p.parseOneExpr()
	if exc != nil {
		return nil, exc
	}
	if p.has_next() {
		switch p.peek().kind {
		case op_token:
			op := p.next()
			next, exc := p.parseExpr()
			if exc != nil {
				return nil, exc
			}
			next_op, ok := next.(*op_expr)
			if ok {
				tmp := &op_expr{exp, next_op, nil, op.content}
				next_op.p = tmp
				exp = tmp.balance()
			} else {
				tmp := &op_expr{exp, next, nil, op.content}
				exp = tmp.balance()
			}
		case comma_token:
			p.next()
			if p.peek().kind == eol_token || p.peek().kind == eof_token {
				return &expr_lst{[]expr{exp}}, nil
			}
			next, exc := p.parseExpr()
			if exc != nil {
				return nil, exc
			}
			next_lst, is_lst := next.(*expr_lst)
			if !is_lst {
				next_lst = &expr_lst{[]expr{next}}
			}
			next_lst.lst = append(next_lst.lst, exp)
			exp = next_lst
		}
	}
	return exp, nil
}

func (p *parser) parseStmt() (stmt, py_exception) {
	var rval stmt
	var exc py_exception
	switch p.peek().kind {
	case if_token:
		rval, exc = p.parseIf()
	case def_token:
		rval, exc = p.parseDef()
	case while_token:
		rval, exc = p.parseWhile()
	case ret_token:
		p.next()
		next, exc := p.parseExpr()
		if exc != nil {
			return nil, exc
		}
		rval = &return_stmt{next}
	case eof_token:
		rval = &eof_stmt{}
	}
	if exc != nil {
		return nil, exc
	}
	if rval != nil {
		if p.has_next() && p.peek().kind == eol_token {
			p.accept(eol_token)
		}
		return rval, exc
	}

	expr, exc := p.parseExpr()
	if exc != nil {
		return nil, exc
	}
	if p.has_next() {
		switch p.peek().kind {
		case eq_token:
			id, is_id := expr.(*identifier_expr)
			if !is_id {
				return nil, raiseException("Invalid assignment statement")
			}
			p.next()
			next, exc := p.parseExpr()
			if exc != nil {
				return nil, exc
			}
			rval = &assign_stmt{id.name, next}
		}
	}
	if p.has_next() && p.peek().kind == eol_token {
		p.next()
	}
	if exc != nil {
		return nil, exc
	}
	if rval != nil {
		return rval, exc
	}
	if expr != nil {
		return expr, exc
	}
	return nil, nil
}

func (p *parser) next() *token {
	var t *token
	if p.next_tok != nil {
		t = p.next_tok
		p.next_tok = nil
	} else {
		t = <-p.lex.items
	}
	if t.kind == eol_token {
		p.has_new = false
	} else {
		p.has_new = true
	}

	return t
}

func (p *parser) next_cont() *token {
	if !p.has_next() {
		p.cont_lex()
	}
	return p.next()
}

func (p *parser) cont_lex() {
	p.lex.cont <- 1
	p.has_new = true
}

func (p *parser) peek() *token {
	if !p.has_next() {
		p.cont_lex()
	}

	return p.peek_wait()
}

func (p *parser) peek_wait() *token {
	if p.next_tok == nil {
		p.next_tok = <-p.lex.items
	}
	return p.next_tok
}

func (p *parser) has_next() bool {
	if p.next_tok != nil {
		return true
	}
	return p.has_new
}

func (p *parser) expect(t tokenType) (*token, py_exception) {
	next_tok := p.peek()
	if next_tok.kind != t {
		return nil, p.unexpectedToken(next_tok, &t)
	}
	return p.next(), nil
}

func (p *parser) accept(t tokenType) bool {
	if !p.has_next() {
		return false
	}

	next_tok := p.peek()
	if next_tok.kind == t {
		p.next()
		return true
	}
	return false
}

func makeParserAndLexer() (p *parser, rval_lex *lexer) {
	lex := makeLexer()
	p = &parser{
		stmts:       make(chan stmt),
		next_tok:    nil,
		lex:         lex,
		has_new:     true,
		should_emit: make(chan bool),
	}
	go p.parse()
	return p, lex
}

func parseNumber(t *token) (e expr, exc py_exception) {
	v, exc := parseNumberToVal(t)
	if exc != nil {
		return nil, exc
	}
	return &number_expr{v}, nil
}

func parseNumberToVal(t *token) (v value, exc py_exception) {
	if t.kind == int_token {
		converted, err := strconv.ParseInt(t.content, 0, 64)
		if err != nil {
			err, ok := err.(*strconv.NumError)
			if !ok {
				panic("wat")
			}
			if err.Err == strconv.ErrRange {
				bigint := big.NewInt(0)
				converted, ok := bigint.SetString(t.content, 0)
				if !ok {
					return nil, raiseException("Invalid long integer")
				}
				return &num_lint_value{value: converted}, nil
			}
			return nil, raiseException("Invalid integer")
		}
		return &num_int_value{value: converted}, nil
	}
	converted, err := strconv.ParseFloat(t.content, 64)
	if err != nil {
		return nil, raiseException("Invalid float")
	}
	return &num_float_value{value: converted}, nil
}

func init() {
	rec = 0
}
