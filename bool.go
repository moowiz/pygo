package main

type py_bool struct {
	value bool
	base_val
}

func (pb *py_bool) get_type() py_type {
	return "bool"
}

func (pb *py_bool) py_string() (string, py_exception) {
	if pb.value {
		return "True", nil
	}
	return "False", nil
}

func (pb *py_bool) true_val() (bool, py_exception) {
	return pb.value, nil
}
