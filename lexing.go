// My implemenation of a lexer for python.
// Heavily inspired by the talk on a lexer given by Rob Pike.
// Link here: http://www.youtube.com/watch?v=HxaD_trXwRE

package main

import (
	"container/list"
	"fmt"
	"regexp"
	"strings"
	"unicode/utf8"
)

type tokenType int

type stateFn func(*lexer) stateFn

type token struct {
	kind    tokenType
	content string
}

type lexer struct {
	in          string
	start       int
	pos         int
	width       int
	items       chan *token
	cont        chan int
	before_line bool
}

var id_re *regexp.Regexp
var ends_with *regexp.Regexp
var eol_s string
var eol rune
var mapping map[string]tokenType

const (
	identifier_token tokenType = iota
	int_token
	float_token
	indent_token
	dedent_token //4
	err_token
	op_token
	lp_token
	rp_token
	comma_token
	cln_token
	eof_token //11
	eol_token
	eq_token
	def_token
	ret_token //15
	if_token
	else_token
	elif_token
	while_token
	identifier_re = "[A-Za-z_]"
	ends_with_re  = "^.*" + identifier_re + "$"
	eof           = '\u0004'
	eof_s         = "\u0004"
)

func (l *lexer) String() string {
	return fmt.Sprintf("start %d pos %d in %s", l.start, l.pos, l.in)
}

func (l *lexer) next() (next rune, s string) {
	if l.pos >= len(l.in) {
		l.width = 0
		return eol, eol_s
	}
	next, l.width = utf8.DecodeRuneInString(l.in[l.pos:])
	s = string(next)
	l.pos += l.width
	return
}

func (l *lexer) backup() {
	l.pos -= l.width
}

func (l *lexer) peek() (r rune, s string) {
	r, s = l.next()
	l.backup()
	return
}

func (l *lexer) ignore() {
	l.start = l.pos
}

func (l *lexer) accept(valid string) bool {
	r, _ := l.next()
	if r == eol {
		l.backup()
		return false
	}
	if strings.IndexRune(valid, r) >= 0 {
		return true
	}
	l.backup()
	return false
}

func (l *lexer) acceptRun(valid string) {
	r, _ := l.next()
	if r == eol {
		l.backup()
		return
	}
	for strings.IndexRune(valid, r) >= 0 {
		r, _ = l.next()
		if r == eol {
			l.backup()
			return
		}
	}
	l.backup()
}

func (l *lexer) curr() (s string) {
	return l.in[l.start:l.pos]
}

func (l *lexer) emit(t tokenType) {
	if t != indent_token && t != dedent_token {
		l.before_line = false
	}
	debug(fmt.Sprintf("Emitting '%s' type %#v start %v", l.curr(), t, l.start), INFO_LEVEL)
	l.items <- &token{t, l.curr()}
	l.start = l.pos
}

func (l *lexer) run() {
	for {
		cont := <-l.cont
		if cont > 0 {
			l.before_line = true
			var new_in string
			if cont > 1 {
				new_in, _ = getInput(">>> ")
			} else {
				new_in, _ = getInput("... ")
			}
			l.in = new_in
		} else {
			continue
		}
		l.start = 0
		l.pos = 0
		for state := lexNorm; state != nil; {
			state = state(l)
		}
	}
}

func lexError(e_msg string) stateFn {
	return func(l *lexer) stateFn {
		l.items <- &token{err_token, l.curr()}
		l.start = l.pos
		return nil
	}
}

func lexSymbol(l *lexer) stateFn {
	for ends_with.FindString(l.curr()) != "" {
		r, rs := l.next()
		if r == eol && rs == eol_s {
			break
		}
	}
	l.backup()
	switch l.curr() {
	case "def":
		l.emit(def_token)
		return lexNorm
	case "return":
		l.emit(ret_token)
		return lexNorm
	case "if":
		l.emit(if_token)
		return lexNorm
	case "else":
		l.emit(else_token)
		return lexNorm
	case "elif":
		l.emit(elif_token)
		return lexNorm
	case "while":
		l.emit(while_token)
		return lexNorm
	}
	for sym := range *get_op_mapping() {
		if sym == l.curr() {
			l.emit(op_token)
			return lexNorm
		}
	}
	l.emit(identifier_token)
	return lexNorm
}

func lexNum(l *lexer) stateFn {
	typ := int_token
	if !strings.Contains("0123456789", l.curr()) {
		l.accept("~+-")
	}

	digits := "0123456789"
	if l.accept("0") && l.accept("xX") {
		digits = "0123456789abcdefABCDEF"
	}
	l.acceptRun(digits)
	if l.accept(".") {
		typ = float_token
		l.acceptRun(digits)
	}

	if l.accept("eE") {
		typ = float_token
		l.accept("+-")
		l.accept("0123456789")
	}

	if _, rs := l.peek(); id_re.FindString(rs) != "" || In(rs, digits) {
		l.next()
		return lexError("??")
	}
	l.emit(typ)
	return lexNorm
}

func (l *lexer) emitOne(typ tokenType) stateFn {
	l.emit(typ)
	return lexNorm
}

func lexOp(l *lexer) stateFn {
	return l.emitOne(op_token)
}

func lexComment(l *lexer) stateFn {
	l.ignore()
	r, _ := l.peek()
	for r != eof || r != eol {
		r, _ = l.next()
	}
	return lexNorm
}

func accumStr(l *lexer, delim string) {
	for strings.HasSuffix(l.curr(), delim) {
		l.next()
	}
	l.backup()
}

var indent_stack *list.List

func indent_level() int {
	rval := 0
	for e := indent_stack.Front(); e != nil; e = e.Next() {
		indent_int, _ := e.Value.(int)
		rval += indent_int
	}
	return rval
}

func lexIndent(l *lexer) stateFn {
	front := l.curr()
	temp := front
	for strings.Contains(temp, "\t") {
		spaces := " "
		temp = strings.Replace(front, "\t", spaces, 1)
		count := -1
		for count%8 != 0 && len(spaces) < 10 {
			temp = strings.Replace(front, "\t", spaces, 1)
			spaces += " "
			count = count_spaces(temp)
		}
		if len(spaces) == 10 {
			return lexError("wat")
		}
		front = temp
	}
	curr := indent_level()
	if len(front) > curr {
		indent_stack.PushBack(len(front) - curr)
		l.emit(indent_token)
	} else if len(front) < curr {
		len_curr := len(front)
		back_val, _ := indent_stack.Back().Value.(int)
		num_dedent := 0
		for back_val != 0 && indent_level() > len_curr {
			elem := indent_stack.Back()
			indent_stack.Remove(elem)
			num_dedent++
		}
		if indent_level() < len_curr {
			return lexError("Weird dedent")
		}
		for ; num_dedent > 0; num_dedent-- {
			l.emit(dedent_token)
		}
	} else {
		l.ignore()
	}
	return lexNorm
}

func lexNorm(l *lexer) stateFn {
	for {
		r, sr := l.next()
		if r == eof && sr == eof_s {
			l.emit(eof_token)
			return nil
		}
		if r == eol && sr == eol_s {
			if len(l.in) <= 1 {
				for e := indent_stack.Back(); e != indent_stack.Front(); {
					tmp := e.Prev()
					indent_stack.Remove(e)
					e = tmp
					l.emit(dedent_token)
				}
			}
			l.emit(eol_token)
			l.before_line = true
			break
		}

		for sym := range *get_op_mapping() {
			if sym == sr {
				return lexOp
			}
			if strings.HasPrefix(sym, sr) {
				_, next_val := l.peek()
				for symm := range *get_op_mapping() {
					if len(symm) == 2 && sr+next_val == symm {
						l.next()
						return lexOp
					}
				}
			}
		}

		if id_re.FindString(sr) != "" {
			return lexSymbol
		}
		v, ok := mapping[sr]
		switch {
		case strings.Contains("+-0123456789", sr):
			return lexNum
		case ok:
			return l.emitOne(v)
		case r == ' ' || r == '\t':
			if l.before_line {
				accumStr(l, sr)
				return lexIndent
			} else {
				l.ignore()
				return lexNorm
			}
		}
		return lexError("Unexpected token")
	}
	return nil
}

func makeLexer() *lexer {
	eol = '\n'
	eol_s = "\n"
	compile_re, err := regexp.Compile(identifier_re)

	indent_stack = list.New()
	indent_stack.PushFront(0)

	if err != nil {
		return nil
	}

	ends_comp_re, err := regexp.Compile(ends_with_re)

	if err != nil {
		return nil
	}

	mapping = map[string]tokenType{
		",": comma_token,
		"(": lp_token,
		")": rp_token,
		":": cln_token,
		"=": eq_token,
	}

	l := &lexer{
		in:    "",
		items: make(chan *token, 10),
		cont:  make(chan int),
	}
	id_re = compile_re
	ends_with = ends_comp_re
	go l.run()
	return l
}
