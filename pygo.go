package main

import (
	"fmt"
	"os"
	//"reflect"
	"flag"
)

var lex *lexer
var parse *parser

func main() {
	var interact = flag.Bool("interact", false, "whether to run the interpreter interactively after loading a file")
    var debug_lev = flag.Int("debug_level", 0, "what level to print out debug messages")
	flag.Parse()
    GLOBAL_debug_level = *debug_lev
	*interact = !(!*interact) //will use this later. Only here to make compiler not complain
	file := ""
	if len(flag.Args()) > 0 {
		file = flag.Arg(0)
	}
	err := InitUtils(file)
	if err != nil {
		fmt.Printf("Error while reading file: %v\n", err)
		return
	}
	global := makeGlobal()
	parse, lex = makeParserAndLexer()
	for {
		lex.cont <- 2
		parse.should_emit <- true
		s := <-parse.stmts
		debug(fmt.Sprintf("FINAL PARSED: %#v", s), DEBUG_LEVEL)
		cont := false

		switch s.(type) {
		case eol_stmt:
			cont = true
		case eof_stmt:
			return
		}

		if cont {
			continue
		}

		err_val, ok := s.(error_stmt)
		if ok {
			handle_exception(err_val.e)
			continue
		}

		val, exc := s.eval(global)
		if exc != nil {
			exit, ok := exc.(*exit_exception)
			if ok {
				os.Exit(exit.status)
			}
			handle_exception(exc)
			continue
		}

		tostr, exc := val.py_string()
		if exc != nil {
			handle_exception(exc)
		} else {
			if tostr != "" {
				fmt.Println(tostr)
			}
		}
	}
}

func handle_exception(exc py_exception) {
	fmt.Printf("Exception: %s\n", exc.get_message())
	select {
	case parse.should_emit <- false:
		break
	default:
		break
	}

}
