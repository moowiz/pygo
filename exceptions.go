package main

type py_exception interface {
	get_message() string
}

type base_exception struct {
	msg string
}

func (this *base_exception) get_message() string {
	return this.msg
}

func raiseException(m string) py_exception {
	return &base_exception{m}
}

type exit_exception struct {
	status int
}

func (this *exit_exception) get_message() string {
	return string(this.status)
}

func exit_python(status int) py_exception {
	return &exit_exception{status}
}
