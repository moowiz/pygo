package main

import "fmt"

type env struct {
	parent  *env
	mapping map[string]value
}

func makeGlobal() (e *env) {
	e = &env{nil, make(map[string]value)}
	for name, val := range builtins {
		e.bind_name(name, val)
	}
	return
}

func (this *env) lookup(name string) (value, py_exception) {
	v, ok := this.mapping[name]
	if ok {
		return v, nil
	}
	if this.parent == nil {
		return nil, raiseException(fmt.Sprintf("name %s is not defined", name))
	}
	return this.parent.lookup(name)
}

func (this *env) bind_name(name string, val value) {
	this.mapping[name] = val
}

func (this *env) extend() *env {
	rval := env{this, make(map[string]value)}
	return &rval
}

func (this *env) get_mapping_string() (s string, exc py_exception) {
	var val string
	for k, v := range this.mapping {
		s += k + ": "
		val, exc = v.py_string()
		if exc != nil {
			return
		}

		s += val + ","
	}
	return
}
