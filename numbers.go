package main

import (
	"fmt"
	"math"
	"math/big"
	"strconv"
)

type num_value interface {
	nrank() num_rank
	value
}

const (
	int_ptype   = "int"
	float_ptype = "float"
	lint_ptype  = "long"
	int_rank    = iota
	lint_rank
	float_rank
	max_int_value = 1 << 62
)

func int_val(nv num_value) (int64, py_exception) {
	switch nv.(type) {
	case *num_int_value:
		inv, _ := nv.(*num_int_value)
		return inv.value, nil
	case *num_float_value:
		fnv, _ := nv.(*num_float_value)
		return int64(fnv.value), nil
	case *num_lint_value:
		linv, _ := nv.(*num_lint_value)
		return linv.value.Int64(), nil
	}
	return 0, raiseException("Invalid type")
}

func float_val(nv num_value) (float64, py_exception) {
	switch nv.(type) {
	case *num_int_value:
		inv, _ := nv.(*num_int_value)
		return float64(inv.value), nil
	case *num_float_value:
		fnv, _ := nv.(*num_float_value)
		return fnv.value, nil
	case *num_lint_value:
		linv, _ := nv.(*num_lint_value)
		return float64(linv.value.Int64()), nil
	}
	return 0.0, raiseException("Invalid type")
}

func lint_val(nv num_value) (*big.Int, py_exception) {
	switch nv.(type) {
	case *num_int_value:
		inv, _ := nv.(*num_int_value)
		return big.NewInt(inv.value), nil
	case *num_float_value:
		fnv, _ := nv.(*num_float_value)
		return big.NewInt(int64(fnv.value)), nil
	case *num_lint_value:
		linv, _ := nv.(*num_lint_value)
		return linv.value, nil
	}
	return nil, raiseException("Invalid type")
}

func do_op(n, v value, op_int func(b int64) value, op_float func(b float64) value, op_lint func(b *big.Int) value) (value, py_exception) {
	num_n, _ := n.(num_value)
	num_v, ok := v.(num_value)
	if !ok {
		return nil, raiseException("Invalid type")
	}
	rank := num_v.nrank()
	if r := num_n.nrank(); rank > r {
		return num_v.add(num_n)
	}
	switch n.(type) {
	case *num_int_value:
		iv, _ := int_val(num_v)
		rval := op_int(iv)
		return rval, nil
	case *num_float_value:
		fv, _ := float_val(num_v)
		rval := op_float(fv)
		return rval, nil
	case *num_lint_value:
		liv, _ := lint_val(num_v)
		rval := op_lint(liv)
		return rval, nil
	}
	return nil, raiseException("Invalid type")
}

type num_int_value struct {
	value int64
	base_val
}

func (n *num_int_value) py_string() (string, py_exception) {
	return strconv.FormatInt(n.value, 10), nil
}

func (n *num_int_value) true_val() (bool, py_exception) {
	return n.value > 0, nil
}

func (n *num_int_value) get_type() py_type {
	return int_ptype
}

func (n *num_int_value) nrank() num_rank {
	return int_rank
}

func (n *num_int_value) negate() (value, py_exception) {
	return &num_int_value{value: -n.value}, nil
}

func (n *num_int_value) not() (value, py_exception) {
	return &num_int_value{value: -(n.value + 1)}, nil
}

func (n *num_int_value) positive() (value, py_exception) {
	return &num_int_value{value: n.value}, nil
}

func (n *num_int_value) add(v value) (value, py_exception) {
	return do_op(n, v, func(b int64) value {
		if (b > 0 && n.value > math.MaxInt64-b) || (b < 0 && n.value < math.MinInt64-b) {
			rval := big.NewInt(n.value)
			cont := num_lint_value{value: rval}
			val_ret, _ := cont.add(v)
			return val_ret
		}
		return &num_int_value{value: n.value + b}
	}, nil, nil)
}

func (n *num_int_value) mul(v value) (value, py_exception) {
	return do_op(n, v, func(b int64) value {
		if temp := (math.MaxInt64 / b) + 1; (b != 1) && (b > 0 && n.value > temp) || (b < 0 && n.value < temp) {
			rval := big.NewInt(n.value)
			cont := num_lint_value{value: rval}
			val_ret, _ := cont.mul(v)
			return val_ret
		}
		return &num_int_value{value: n.value * b}
	}, nil, nil)
}

func (n *num_int_value) div(v value) (value, py_exception) {
	return do_op(n, v, func(b int64) value {
		return &num_int_value{value: n.value / b}
	}, nil, nil)
}

func (n *num_int_value) sub(v value) (value, py_exception) {
	return do_op(n, v, func(b int64) value {
		if (b < 0 && n.value > math.MinInt64-b) || (b > 0 && n.value > math.MaxInt64-b) {
			rval := big.NewInt(n.value)
			cont := num_lint_value{value: rval}
			val_ret, _ := cont.sub(v)
			return val_ret
		}
		return &num_int_value{value: n.value - b}
	}, nil, nil)
}

func (n *num_int_value) eq(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, func(b int64) value {
		return &py_bool{value: n.value == b}
	}, nil, nil)
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

func (n *num_int_value) ne(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, func(b int64) value {
		return &py_bool{value: n.value != b}
	}, nil, nil)
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

func (n *num_int_value) lt(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, func(b int64) value {
		return &py_bool{value: n.value < b}
	}, nil, nil)
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

func (n *num_int_value) gt(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, func(b int64) value {
		return &py_bool{value: n.value > b}
	}, nil, nil)
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

func (n *num_int_value) le(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, func(b int64) value {
		return &py_bool{value: n.value <= b}
	}, nil, nil)
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

func (n *num_int_value) ge(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, func(b int64) value {
		return &py_bool{value: n.value >= b}
	}, nil, nil)
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

type num_float_value struct {
	value float64
	base_val
}

func (n *num_float_value) get_type() py_type {
	return float_ptype
}

func (n *num_float_value) py_string() (string, py_exception) {
	return fmt.Sprintf("%g", n.value), nil
}

func (n *num_float_value) true_val() (bool, py_exception) {
	return n.value > 0, nil
}

func (n *num_float_value) nrank() num_rank {
	return float_rank
}

func (n *num_float_value) negate() (value, py_exception) {
	return &num_float_value{value: -n.value}, nil
}

func (n *num_float_value) not() (value, py_exception) {
	return nil, raiseException("Invalid type")
}

func (n *num_float_value) positive() (value, py_exception) {
	return &num_float_value{value: n.value}, nil
}

func (n *num_float_value) add(v value) (value, py_exception) {
	return do_op(n, v, nil, func(b float64) value {
		return &num_float_value{value: n.value + b}
	}, nil)
}

func (n *num_float_value) mul(v value) (value, py_exception) {
	return do_op(n, v, nil, func(b float64) value {
		return &num_float_value{value: n.value * b}
	}, nil)
}

func (n *num_float_value) div(v value) (value, py_exception) {
	return do_op(n, v, nil, func(b float64) value {
		return &num_float_value{value: n.value / b}
	}, nil)
}

func (n *num_float_value) sub(v value) (value, py_exception) {
	return do_op(n, v, nil, func(b float64) value {
		return &num_float_value{value: n.value - b}
	}, nil)
}

func (n *num_float_value) eq(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, nil, func(b float64) value {
		return &py_bool{value: n.value == b}
	}, nil)
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

func (n *num_float_value) ne(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, nil, func(b float64) value {
		return &py_bool{value: n.value != b}
	}, nil)
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

func (n *num_float_value) lt(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, nil, func(b float64) value {
		return &py_bool{value: n.value < b}
	}, nil)
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

func (n *num_float_value) gt(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, nil, func(b float64) value {
		return &py_bool{value: n.value > b}
	}, nil)
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

func (n *num_float_value) le(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, nil, func(b float64) value {
		return &py_bool{value: n.value <= b}
	}, nil)
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

func (n *num_float_value) ge(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, nil, func(b float64) value {
		return &py_bool{value: n.value >= b}
	}, nil)
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

type num_lint_value struct {
	value *big.Int
	base_val
}

func (n *num_lint_value) get_type() py_type {
	return lint_ptype
}

func (n *num_lint_value) nrank() num_rank {
	return lint_rank
}

func (n *num_lint_value) py_string() (string, py_exception) {
	return n.value.String(), nil
}

func (n *num_lint_value) true_val() (bool, py_exception) {
	return n.value.Cmp(big.NewInt(0)) > 0, nil
}

func (n *num_lint_value) negate() (value, py_exception) {
	rval := big.NewInt(0)
	rval.Neg(n.value)
	return &num_lint_value{value: rval}, nil
}

func (n *num_lint_value) not() (value, py_exception) {
	rval := big.NewInt(0)
	rval.Not(n.value)
	return &num_lint_value{value: rval}, nil
}

func (n *num_lint_value) positive() (value, py_exception) {
	rval := big.NewInt(0)
	rval.Add(rval, n.value)
	return &num_lint_value{value: rval}, nil
}

func (n *num_lint_value) add(v value) (value, py_exception) {
	return do_op(n, v, nil, nil, func(b *big.Int) value {
		rval := big.NewInt(0)
		rval = rval.Add(n.value, b)
		return &num_lint_value{value: rval}
	})
}

func (n *num_lint_value) mul(v value) (value, py_exception) {
	return do_op(n, v, nil, nil, func(b *big.Int) value {
		rval := big.NewInt(0)
		rval = rval.Mul(n.value, b)
		return &num_lint_value{value: rval}
	})
}

func (n *num_lint_value) div(v value) (value, py_exception) {
	return do_op(n, v, nil, nil, func(b *big.Int) value {
		rval := big.NewInt(0)
		rval = rval.Div(n.value, b)
		return &num_lint_value{value: rval}
	})
}

func (n *num_lint_value) sub(v value) (value, py_exception) {
	return do_op(n, v, nil, nil, func(b *big.Int) value {
		rval := big.NewInt(0)
		rval = rval.Sub(n.value, b)
		return &num_lint_value{value: rval}
	})
}

func (n *num_lint_value) eq(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, nil, nil, func(b *big.Int) value {
		return &py_bool{value: n.value.Cmp(b) == 0}
	})
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

func (n *num_lint_value) ne(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, nil, nil, func(b *big.Int) value {
		return &py_bool{value: n.value.Cmp(b) != 0}
	})
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

func (n *num_lint_value) lt(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, nil, nil, func(b *big.Int) value {
		return &py_bool{value: n.value.Cmp(b) < 0}
	})
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

func (n *num_lint_value) gt(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, nil, nil, func(b *big.Int) value {
		return &py_bool{value: n.value.Cmp(b) > 0}
	})
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

func (n *num_lint_value) le(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, nil, nil, func(b *big.Int) value {
		val := n.value.Cmp(b)
		return &py_bool{value: val <= 0}
	})
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}

func (n *num_lint_value) ge(v value) (*py_bool, py_exception) {
	val, exc := do_op(n, v, nil, nil, func(b *big.Int) value {
		val := n.value.Cmp(b)
		return &py_bool{value: val >= 0}
	})
	if exc != nil {
		return nil, exc
	}
	conv, _ := val.(*py_bool)
	return conv, nil
}
