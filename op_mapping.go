package main

const (
	mul_rank = iota
	add_rank
	shift_rank
	b_and_rank
	b_xor_rank
	b_or_rank
	comp_rank
	not_rank
	and_rank
	or_rank
)

var op_mapping map[string]int

func get_op_mapping() *map[string]int {
	return &op_mapping
}

func init() {
	op_mapping = make(map[string]int)
	add_mp := func(s string, i int) {
		op_mapping[s] = i
	}
	add_mp("*", mul_rank)
	add_mp("//", mul_rank)
	add_mp("/", mul_rank)
	add_mp("%", mul_rank)
	add_mp("+", add_rank)
	add_mp("-", add_rank)
	add_mp("<<", shift_rank)
	add_mp(">>", shift_rank)
	add_mp("&", b_and_rank)
	add_mp("^", b_xor_rank)
	add_mp("|", b_or_rank)
	add_mp("<", comp_rank)
	add_mp(">", comp_rank)
	add_mp("<=", comp_rank)
	add_mp(">=", comp_rank)
	add_mp("==", comp_rank)
	add_mp("!=", comp_rank)
	add_mp("is", comp_rank)
	add_mp("in", comp_rank)
	add_mp("or", or_rank)
	add_mp("and", and_rank)
}
