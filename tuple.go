package main

const (
	tuple_ptype = "tuple"
)

type tuple_val struct {
	vals []value
	base_val
}

func (this *tuple_val) String() string {
	rval, _ := this.py_string()
	return rval
}

func (this *tuple_val) get_type() py_type {
	return tuple_ptype
}

func (this *tuple_val) add(value) (value, py_exception) {
	return nil, raiseException("Invalid operation")
}

func (this *tuple_val) mul(value) (value, py_exception) {
	return nil, raiseException("Invalid operation")
}

func (this *tuple_val) py_string() (string, py_exception) {
	s := "("
	for _, v := range this.vals {
		tos, exc := v.py_string()
		if exc != nil {
			return "", exc
		}
		s += tos + ", "
	}
	if len(this.vals) > 1 {
		s = s[:len(s)-2]
	}
	return s + ")", nil
}

func (this *tuple_val) true_val() (bool, py_exception) {
	return len(this.vals) > 0, nil
}
