package main

type stmt interface {
	eval(*env) (value, py_exception)
}

type assign_stmt struct {
	name string
	exp  expr
}

type return_stmt struct {
	exp expr
}

func (this *assign_stmt) eval(e *env) (value, py_exception) {
	evaled, exc := this.exp.eval(e)
	if exc != nil {
		return nil, exc
	}
	e.bind_name(this.name, evaled)
	return the_py_none, nil
}

func (this *return_stmt) eval(e *env) (value, py_exception) {
	if e.parent == nil {
		return nil, raiseException("Cannot return outside of a function")
	}
	evaled, exc := this.exp.eval(e)
	if exc != nil {
		return nil, exc
	}
	return &return_val{val: evaled}, exc
}

type eof_stmt struct{}

func (es eof_stmt) eval(e *env) (value, py_exception) {
	return &py_none{}, nil
}

type eol_stmt struct{}

func (es eol_stmt) eval(e *env) (value, py_exception) {
	return &py_none{}, nil
}

type error_stmt struct {
	e py_exception
}

func (es error_stmt) eval(e *env) (value, py_exception) {
	return nil, es.e
}

type if_stmt struct {
	suites     []*suite
	tests      []expr
	else_suite *suite
}

func (this *if_stmt) eval(e *env) (value, py_exception) {
	for i, test := range this.tests {
		evaled_test, exc := test.eval(e)
		if exc != nil {
			return nil, exc
		}

		val, exc := evaled_test.true_val()
		if exc != nil {
			return nil, exc
		}
		if val {
			rval, exc := this.suites[i].eval(e)
			if exc != nil {
				return nil, exc
			}

			_, ok := rval.(*return_val)
			if ok {
				return rval, nil
			}
		}
	}
	if this.else_suite != nil {
		val, exc := this.else_suite.eval(e)
		if exc != nil {
			return nil, exc
		}

		_, ok := val.(*return_val)
		if ok {
			return val, nil
		}
	}
	return the_py_none, nil
}

type suite struct {
	stmts []stmt
}

func (this *suite) eval(e *env) (value, py_exception) {
	for _, v := range this.stmts {
		val, exc := v.eval(e)
		if exc != nil {
			return nil, exc
		}

		conv, ok := val.(*return_val)
		if ok {
			return conv, nil
		}
	}
	return the_py_none, nil
}

type while_stmt struct {
	test expr
	body *suite
}

func (this *while_stmt) eval(e *env) (value, py_exception) {
	do_thing := func() (bool, py_exception) {
		test, exc := this.test.eval(e)
		if exc != nil {
			return false, exc
		}

		is_true, exc := test.true_val()
		return is_true, exc
	}
	for {
		_, exc := e.get_mapping_string()
		if exc != nil {
			return nil, exc
		}
		val, exc := do_thing()
		if exc != nil {
			return nil, exc
		}
		if !val {
			return the_py_none, nil
		}

		rval, exc := this.body.eval(e)
		if exc != nil {
			return nil, exc
		}

		conv, ok := rval.(*return_val)
		if ok {
			return conv, nil
		}
	}
	return the_py_none, nil
}
