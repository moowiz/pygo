package main

import (
	"container/list"
	"fmt"
)

type expr interface {
	eval(*env) (value, py_exception)
	fmt.Stringer
}

type identifier_expr struct {
	name string
}

func (this *identifier_expr) eval(e *env) (value, py_exception) {
	return e.lookup(this.name)
}

func (this *identifier_expr) String() string {
	return this.name
}

type number_expr struct {
	val value
}

func (this *number_expr) String() string {
	val, _ := this.val.py_string()
	return val
}

func (ne number_expr) eval(e *env) (value, py_exception) {
	return ne.val, nil
}

type op_expr struct {
	a, b expr
	p    *op_expr
	op   string
}

type expr_lst struct {
	lst []expr
}

func (this *expr_lst) String() string {
	s := "("
	for _, v := range this.lst {
		tos := v.String()
		s += tos + ", "
	}
	if len(this.lst) > 1 {
		s = s[:len(s)-2]
	}
	return s + ")"

}

func (lst *expr_lst) eval(e *env) (value, py_exception) {
	rval := &tuple_val{vals: nil}
	rval.vals = make([]value, len(lst.lst))
	for i, item := range lst.lst {
		new_val, exc := item.eval(e)
		if exc != nil {
			return nil, exc
		}
		rval.vals[len(lst.lst)-i-1] = new_val
	}
	return rval, nil
}

type paren_expr struct {
	val expr
}

func (pe *paren_expr) eval(e *env) (value, py_exception) {
	return pe.val.eval(e)
}

func bubble_up(o *op_expr) {
	debug(fmt.Sprintf("bubbling up %v\n", o), INFO_LEVEL)
	if o.p == nil {
		return
	}
	for op_mapping[o.p.op] < op_mapping[o.op] {
		parent := o.p

		o.p = parent.p
		opa, ok := parent.a.(*op_expr)
		if ok && opa == o { //rotate right
			orphaned := o.b

			o.b = parent
			parent.a = orphaned
			o_op, ok := orphaned.(*op_expr)
			if ok {
				o_op.p = parent
			}
		} else { //rotate left
			orphaned := o.a

			o.a = parent
			parent.b = orphaned
			o_op, ok := orphaned.(*op_expr)
			if ok {
				o_op.p = parent
			}
		}
		if parent.p != nil {
			if parent.p.b == parent {
				parent.p.b = o
			} else {
				parent.p.a = o
			}
		}
		parent.p = o
		if o.p == nil {
			return
		}
	}
}

func (o *op_expr) balance() *op_expr {
	debug(fmt.Sprintf("balance %v", o), INFO_LEVEL)
	q := list.New()
	q.PushFront(o)
	lst := list.New()
	for q.Len() > 0 {
		elem := q.Front()
		q.Remove(elem)
		lst.PushBack(elem.Value)
		elem_op, _ := elem.Value.(*op_expr)
		left, ok := elem_op.a.(*op_expr)
		if ok {
			q.PushFront(left)
		}
		right, ok := elem_op.b.(*op_expr)
		if ok {
			q.PushFront(right)
		}
	}
	for lst.Len() > 0 {
		elem := lst.Back()
		lst.Remove(elem)
		elem_op, _ := elem.Value.(*op_expr)
		bubble_up(elem_op)
	}
	for o.p != nil {
		o = o.p
	}
	debug(fmt.Sprintf("done balancing %v", o), INFO_LEVEL)
	return o
}

func (o *op_expr) String() string {
	return o.a.String() + " " + o.op + " " + o.b.String()
}

func (o *op_expr) eval(e *env) (rval value, exc py_exception) {
	debug(fmt.Sprintf("Evaling %v", *o), INFO_LEVEL)
	a_eval, exc := o.a.eval(e)
	rval = nil
	if exc != nil {
		return nil, exc
	}

	b_eval, exc := o.b.eval(e)
	if exc != nil {
		return nil, exc
	}
	switch o.op {
	case "+":
		rval, exc = a_eval.add(b_eval)
	case "-":
		rval, exc = a_eval.sub(b_eval)
	case "/":
		rval, exc = a_eval.div(b_eval)
	case "*":
		rval, exc = a_eval.mul(b_eval)
	case "and":
		atv, exc := a_eval.true_val()
		if exc != nil {
			return nil, exc
		}
		if !atv {
			rval, exc = &py_bool{value: false}, nil
		}
		btv, exc := b_eval.true_val()
		if exc != nil {
			return nil, exc
		}
		rval, exc = &py_bool{value: btv}, nil
	case "or":
		atv, exc := a_eval.true_val()
		if exc != nil {
			return nil, exc
		}
		if atv {
			rval, exc = &py_bool{value: true}, nil
		}
		btv, exc := b_eval.true_val()
		if exc != nil {
			return nil, exc
		}
		rval, exc = &py_bool{value: btv}, nil
	case "==":
		rval, exc = a_eval.eq(b_eval)
	case "!=":
		rval, exc = a_eval.ne(b_eval)
	case "<":
		rval, exc = a_eval.lt(b_eval)
	case ">":
		rval, exc = a_eval.gt(b_eval)
	case "<=":
		rval, exc = a_eval.le(b_eval)
	case ">=":
		rval, exc = a_eval.ge(b_eval)

	}
	if rval != nil {
		return
	}
	return nil, raiseException("Undefined operator")
}
