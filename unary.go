package main

type unary_expr struct {
	target expr
	op     string
}

func (ue unary_expr) eval(e *env) (value, py_exception) {
	v, exc := ue.target.eval(e)
	if exc != nil {
		return nil, exc
	}
	switch ue.op {
	case "~":
		return v.not()
	case "-":
		return v.negate()
	case "+":
		return v.positive()
	}
	return nil, raiseException("Unrecognized op")
}
