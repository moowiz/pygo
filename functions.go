package main

import "fmt"

type def_stmt struct {
	id   *identifier_expr
	args *args_lst
	body *suite
}

type call_expr struct {
	exp  expr
	args *args_lst
}

func (this *call_expr) String() string {
	return this.exp.String() + this.args.String()
}

func (this *call_expr) eval(e *env) (value, py_exception) {
	evaled, exc := this.exp.eval(e)
	if exc != nil {
		return nil, exc
	}
	if this.args != nil && this.args.len() > 0 {
		values := make([]value, this.args.len())
		for i, v := range this.args.items.lst {
			v_eval, exc := v.eval(e)
			if exc != nil {
				return nil, exc
			}
			values[i] = v_eval
		}
		return evaled.call(values)
	}
	return evaled.call(nil)
}

type function_val struct {
	base_val
	args         *args_lst
	body         *suite
	parent_frame *env
}

func (this *function_val) get_type() py_type {
	return "function"
}

func (this *function_val) py_string() (string, py_exception) {
	return "function", nil
}

func (this *function_val) true_val() (bool, py_exception) {
	return true, nil
}

func (this *function_val) call(vals []value) (value, py_exception) {
	e := this.parent_frame.extend()
	for i, v := range vals {
		it := this.args.items.lst[i]
		switch it.(type) {
		case *identifier_expr:
			conv, _ := it.(*identifier_expr)
			e.bind_name(conv.name, v)
		default:
			return nil, raiseException(fmt.Sprintf("Bad argument name %#v in function call", it))
		}
	}
	rval, exc := this.body.eval(e)
	if rval == nil {
		return the_py_none, exc
	}
	conv, ok := rval.(*return_val)
	if ok {
		return conv.val, exc
	}
	return nil, raiseException(fmt.Sprintf("Body returned a weird value %#v\n", rval))
}

func (this *def_stmt) eval(e *env) (value, py_exception) {
	e.bind_name(this.id.name, &function_val{args: this.args,
		body:         this.body,
		parent_frame: e})
	return the_py_none, nil
}

type args_lst struct {
	items *expr_lst
}

func (this *args_lst) len() int {
	if this.items == nil {
		return 0
	}
	return len(this.items.lst)
}

func (this *args_lst) String() string {
	return this.items.String()
}

type builtin_func struct {
	function func([]value) (value, py_exception)
	e        *env
	base_val
}

func (this *builtin_func) get_type() py_type {
	return "builtin function"
}

func (this *builtin_func) py_string() (string, py_exception) {
	return "builtin function", nil
}

func (this *builtin_func) call(vals []value) (value, py_exception) {
	return this.function(vals)
}
