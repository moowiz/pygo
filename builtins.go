package main

import (
	"fmt"
)

var builtins map[string]*builtin_func

type exit_val struct {
	status int
	base_val
}

func (this *exit_val) get_type() py_type {
	return ""
}

func (this *exit_val) py_string() (string, py_exception) {
	return "Use exit() or Ctrl-D (i.e. EOF) to exit\n", nil
}

func py_exit(msg []value) (value, py_exception) {
	if len(msg) > 0 {
		return nil, exit_python(1)
	}
	return nil, exit_python(0)
}

func py_print(values []value) (value, py_exception) {
	s := ""
	for _, v := range values {
		temp, exc := v.py_string()
		if exc != nil {
			return nil, exc
		}

		s += temp
	}
	s += "\n"
	fmt.Print(s)
	return the_py_none, nil
}

func init() {
	builtins = make(map[string]*builtin_func)
	builtins["print"] = &builtin_func{function: py_print}
	builtins["exit"] = &builtin_func{function: py_exit}
}
