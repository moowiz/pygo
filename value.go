package main

type value interface {
	get_type() py_type
	add(value) (value, py_exception)
	mul(value) (value, py_exception)
	sub(value) (value, py_exception)
	div(value) (value, py_exception)
	py_string() (string, py_exception)
	true_val() (bool, py_exception)
	negate() (value, py_exception)
	not() (value, py_exception)
	positive() (value, py_exception)
	call([]value) (value, py_exception)
	eq(value) (*py_bool, py_exception)
	ne(value) (*py_bool, py_exception)
	lt(value) (*py_bool, py_exception)
	gt(value) (*py_bool, py_exception)
	le(value) (*py_bool, py_exception)
	ge(value) (*py_bool, py_exception)
}

type decl_val struct {
	typ     py_type
	add_fn  func(value) (value, py_exception)
	mul_fn  func(value) (value, py_exception)
	sub_fn  func(value) (value, py_exception)
	div_fn  func(value) (value, py_exception)
	call_fn func([]value) (value, py_exception)
}

type return_val struct {
	val value
	base_val
}

func (this *return_val) get_type() py_type {
	return "return_val"
}

func (this *return_val) py_string() (string, py_exception) {
	return "", raiseException("Shouldn't ever call this")
}

type base_val struct{}

func (this *base_val) add(value) (value, py_exception) {
	return nil, raiseException("Invalid operation")
}

func (this *base_val) mul(value) (value, py_exception) {
	return nil, raiseException("Invalid operation")
}

func (this *base_val) sub(value) (value, py_exception) {
	return nil, raiseException("Invalid operation")
}

func (this *base_val) div(value) (value, py_exception) {
	return nil, raiseException("Invalid operation")
}

func (this *base_val) true_val() (bool, py_exception) {
	return false, raiseException("Invalid operation")
}

func (this *base_val) negate() (value, py_exception) {
	return nil, raiseException("Invalid operation")
}

func (this *base_val) not() (value, py_exception) {
	return nil, raiseException("Invalid operation")
}

func (this *base_val) positive() (value, py_exception) {
	return nil, raiseException("Invalid operation")
}

func (this *base_val) call(vals []value) (value, py_exception) {
	return nil, raiseException("Invalid operation")
}

func (this *base_val) eq(v value) (*py_bool, py_exception) {
	return nil, raiseException("Invalid operation")
}

func (this *base_val) ne(v value) (*py_bool, py_exception) {
	return nil, raiseException("Invalid operation")
}

func (this *base_val) lt(v value) (*py_bool, py_exception) {
	return nil, raiseException("Invalid operation")
}

func (this *base_val) gt(v value) (*py_bool, py_exception) {
	return nil, raiseException("Invalid operation")
}

func (this *base_val) le(v value) (*py_bool, py_exception) {
	return nil, raiseException("Invalid operation")
}

func (this *base_val) ge(v value) (*py_bool, py_exception) {
	return nil, raiseException("Invalid operation")
}

type py_none struct {
	base_val
}

func (this *py_none) get_type() py_type {
	return "None"
}

func (this *py_none) py_string() (string, py_exception) {
	return "", nil
}

func (this *py_none) true_val() (bool, py_exception) {
	return false, nil
}

var the_py_none value

type py_type string

type num_rank int

func init() {
	the_py_none = &py_none{}
}
